node {
    properties([
        parameters([
            choice(choices: ['info', 'warning', 'debug'], 
            name: 'LIQUIBASE_LOG_LEVEL'), 
            string(defaultValue: '172.17.0.3', name: 'PG_HOST'), 
            string(defaultValue: '5432', name: 'PG_PORT'),
            string(defaultValue: 'postgres', name: 'PG_DATABASE'),
            string(defaultValue: 'public', name: 'PG_SCHEMA'),
        ])
    ])
    def liquibase
    stage('Initial build') {
        def changeLogFile = 'liquibase/examples/sql/samplechangelog.h2.sql'
        def username = 'postgres'
        def password = 'mysecretpassword'
        def url = "jdbc:postgresql://${env.PG_HOST}:${env.PG_PORT}/${env.PG_DATABASE}?currentSchema=${env.PG_SCHEMA}"
        def logLevel = "${LIQUIBASE_LOG_LEVEL}"

        sh """cat <<EOF > postgres.liquibase.properties
        changeLogFile=${changeLogFile}
        url=${url}
        username=${username}
        password=${password}
        logLevel=${logLevel}
        """

        liquibase = docker.image('liquibase/liquibase:4.4')
        liquibase.pull()
    }
    
    stage('Postgres Migration') {
        liquibase.inside() {
            sh 'which liquibase'

            sh 'cp -r /liquibase .'
            
            sh "liquibase --defaultsFile=postgres.liquibase.properties update"
        }
    }
}