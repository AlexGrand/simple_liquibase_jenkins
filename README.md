# simple_liquibase_jenkins

Jenkins in docker + Liquibase migrations on separate postgres (in docker)

1. Build Jenkins image:
```docker
docker build -t jenkins-with-docker -f docker/Dockerfile .

```
2. Run Jenkins:
```docker
docker run -d -p 8080:8080 --name jenkins-with-docker -v /var/run/docker.sock:/var/run/docker/sock jenkins-with-docker
```
3. Copy paste `initialAdminPassword`:
```docker
docker exec -it jenkins-with-docker cat /var/jenkins_home/secrets/initialAdminPassword
```
4. Install default plugins
5. Run your job with provided Jenkinsfile
